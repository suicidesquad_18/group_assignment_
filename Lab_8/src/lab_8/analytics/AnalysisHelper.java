/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {

    public void userWithMostLikes() {
        Map<Integer, Integer> userLikeCount = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();

        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                int likes = 0;
                if (userLikeCount.containsKey(user.getId())) {
                    likes = userLikeCount.get(user.getId());
                }
                likes += c.getLikes();
                userLikeCount.put(user.getId(), likes);
            }
        }

        int max = 0;
        int maxId = 0;
        for (int id : userLikeCount.keySet()) {
            if (userLikeCount.get(id) > max) {
                max = userLikeCount.get(id);
                maxId = id;   
            }            
        }

        User winner = users.get(maxId);
        System.out.println("\nUser with most likes : " + winner.getFirstName() + " " + winner.getLastName());
        System.out.println("Number of likes : " + userLikeCount.get(maxId) + "\n");
    }

    public void getFiveMostLikedComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());

        Collections.sort(commentList, new Comparator<Comment>() {
            @Override
            // descending order
            public int compare(Comment o1, Comment o2) {
                return o2.getLikes() - o1.getLikes();
            }
        });
                
        System.out.println("5 most liked comments : ");
        for (int i = 0; i < commentList.size() && i < 5; i++) {
            System.out.println(commentList.get(i));
            System.out.println("Number : " + commentList.get(i).getLikes());
        }
        System.out.println();
    }

    public void getAverageLikesPerComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());

        int likes = 0;
        for (Comment c : commentList) {
            likes += c.getLikes();
        }
        
        if (commentList.isEmpty()) {
            System.out.println("No comments data");
            return;
        }
        float average = likes / commentList.size();
        System.out.println("Average likes per comment : " + average + "\n");
    }

    public void getMostLikedPost() {
        Map<Integer, Integer> postLikeCount = new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();

        for (Post post : posts.values()) {
            for (Comment c : post.getComments()) {
                int likes = 0;
                if (postLikeCount.containsKey(post.getPostId())) {
                    likes = postLikeCount.get(post.getPostId());
                }
                likes += c.getLikes();
                postLikeCount.put(post.getPostId(), likes);
            }
        }

        int max = 0;
        int maxId = 0;
        for (int id : postLikeCount.keySet()) {
            if (postLikeCount.get(id) > max) {
                max = postLikeCount.get(id);
                maxId = id;
            }            
        }
        
        Post winner = posts.get(maxId);
        System.out.println("Post with most likes : " + winner.getPostId());
        System.out.println("Number of likes : " + postLikeCount.get(maxId) + "\n");
    }

    public void getMostCommentsPost() {
        Map<Integer, Integer> postCommentCount = new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();

        for (Post post : posts.values()) {
            int comments = 0;
            if (postCommentCount.containsKey(post.getPostId())) {
                comments = postCommentCount.get(post.getPostId());
            }
            comments += post.getComments().size();
            postCommentCount.put(post.getPostId(), comments);

        }

        int max = 0;
        int maxId = 0;
        for (int id : postCommentCount.keySet()) {
            if (postCommentCount.get(id) > max) {
                max = postCommentCount.get(id);
                maxId = id;   
            }
        }
        
        Post winner = posts.get(maxId);
        System.out.println("Post with most comments : " + winner.getPostId());
        System.out.println("Number of comments : " + postCommentCount.get(maxId) + "\n");
    }

    public void getFiveLeastPostUser() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> userPostCount = new HashMap<>();
        List<User> userList = new ArrayList<>(users.values());
        
        for (Post post : posts.values()) {
            int userId = post.getUserId();
            int nums = userPostCount.containsKey(userId) ? userPostCount.get(userId) : 0;
            userPostCount.put(post.getUserId(), nums + 1);
        }
                
        Collections.sort(userList, new Comparator<User>() {
            @Override
            // ascending order
            public int compare(User o1, User o2) {
                if (!userPostCount.containsKey(o1.getId())) {
                    return -1;
                }
                if (!userPostCount.containsKey(o2.getId())) {
                    return 1;
                }
                return userPostCount.get(o1.getId()) - userPostCount.get(o2.getId());
            }
        });

        System.out.println("5 inactive users based on posts : ");
        for (int i = 0; i < userList.size() && i < 5; i++) {
            System.out.println(userList.get(i));
            System.out.println("Number : " + userPostCount.get(userList.get(i).getId()));
        }
        System.out.println();
    }

    public void getFiveLeastCommentUser() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        List<User> userList = new ArrayList<>(users.values());

        Collections.sort(userList, new Comparator<User>() {
            @Override
            // ascending order
            public int compare(User o1, User o2) {
                return o1.getComments().size() - o2.getComments().size();
            }
        });

        System.out.println("5 inactive users based on comments : ");
        for (int i = 0; i < userList.size() && i < 5; i++) {
            System.out.println(userList.get(i));
            System.out.println("Number : " + userList.get(i).getComments().size());
        }
        System.out.println();
    }

    public void getFiveLeastActiveUser() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> userOverallCount = new HashMap<>();
        List<User> userList = new ArrayList<>(users.values());
        
        for (Post post : posts.values()) {
            int userId = post.getUserId();
            int nums = userOverallCount.containsKey(userId) ? userOverallCount.get(userId) : 0;
            userOverallCount.put(post.getUserId(), nums + 1);
        }
        
        for (User user : users.values()) {
            int userId = user.getId();
            int nums = userOverallCount.containsKey(userId) ? userOverallCount.get(userId) : 0;
            // comments
            nums += user.getComments().size();
            // likes
            for (Comment c : user.getComments()) {
                nums += c.getLikes();
            }
            userOverallCount.put(userId, nums);
        }

        Collections.sort(userList, new Comparator<User>() {
            @Override
            // ascending order
            public int compare(User o1, User o2) {
                return userOverallCount.get(o1.getId()) - userOverallCount.get(o2.getId());
            }
        });

        System.out.println("5 inactive users overall : ");
        for (int i = 0; i < userList.size() && i < 5; i++) {
            System.out.println(userList.get(i));
            System.out.println("Number : " + userOverallCount.get(userList.get(i).getId()));
        }
        System.out.println();
    }

    public void getFiveMostActiveUser() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> userOverallCount = new HashMap<>();
        List<User> userList = new ArrayList<>(users.values());
        
        for (Post post : posts.values()) {
            int userId = post.getUserId();
            int nums = userOverallCount.containsKey(userId) ? userOverallCount.get(userId) : 0;
            userOverallCount.put(post.getUserId(), nums + 1);
        }
        
        for (User user : users.values()) {
            int userId = user.getId();
            int nums = userOverallCount.containsKey(userId) ? userOverallCount.get(userId) : 0;
            // comments
            nums += user.getComments().size();
            // likes
            for (Comment c : user.getComments()) {
                nums += c.getLikes();
            }
            userOverallCount.put(userId, nums);
        }

        Collections.sort(userList, new Comparator<User>() {
            @Override
            // descending order
            public int compare(User o1, User o2) {
                return userOverallCount.get(o2.getId()) - userOverallCount.get(o1.getId());
            }
        });

        System.out.println("5 proactive users overall : ");
        for (int i = 0; i < userList.size() && i < 5; i++) {
            System.out.println(userList.get(i));
            System.out.println("Number : " + userOverallCount.get(userList.get(i).getId()));
        }
        System.out.println();
    }

}
