/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analytics;

import java.util.HashMap;
import java.util.Map;
import assignment_4.DataStore;
import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author littlelion
 */
public class AnalysisHelper {
    public void top3PopularProduct(){
        System.out.println("The top 3 most popular product is:");
        
        Map<Integer,Integer> productQuantity =new HashMap<>();
        Map<Integer,Order> orders = DataStore.getInstance().getOrder();
       
        for(Order order : orders.values()){
            
            Item item =order.getItem();
            int totalQuantity =0;
            if(productQuantity.containsKey(item.getProductId())){
                totalQuantity=productQuantity.get(item.getProductId());
            }
            totalQuantity += item.getQuantity();
            productQuantity.put(item.getProductId(), totalQuantity);
            
        }
        
        List<Map.Entry<Integer,Integer>> productList =new ArrayList<Map.Entry<Integer,Integer>>(productQuantity.entrySet());
        Collections.sort(productList,new Comparator<Map.Entry<Integer,Integer>>(){
            public int compare(Map.Entry<Integer, Integer> o1,  
                    Map.Entry<Integer, Integer> o2) {  
                return (o1.getValue()).toString().compareTo(o2.getValue().toString());
                
            }
        });
        
        int n =0;
        for(int i=productList.size()-1;i>=0;i--){
            if(n<3){
                n++;
                System.out.println("Top "+n+" product is "+productList.get(i).getKey()+",and it sales "+productList.get(i).getValue()+".");
                continue;
            }else if(n==3){
                if(Objects.equals(productList.get(i).getValue(), productList.get(i+1).getValue())){
                   System.out.println("Top "+n+" product is "+productList.get(i).getKey()+",and it sales "+productList.get(i).getValue()+".");
                }
                else break;
            }
            
        }
        System.out.println("-------------------------------------------------------------");
    }
    
    public void top3Customer(){
         System.out.println("The top 3 customer is:");
        Map<Customer,Integer> customerMoney = new HashMap<Customer,Integer>();
        Map<Integer,Customer> customerMap=DataStore.getInstance().getCustomer();
        for(Customer customer:customerMap.values()){
            int money=0;
            for(Order order:customer.getOrders()){
                money += order.getItem().getQuantity()*order.getItem().getSalesPrice();
            }
            customerMoney.put(customer, money);
        }
        
        List<Map.Entry<Customer,Integer>> customerMoneyList =new ArrayList<Map.Entry<Customer,Integer>>(customerMoney.entrySet());
        Collections.sort(customerMoneyList,new Comparator<Map.Entry<Customer,Integer>>(){
            public int compare(Map.Entry<Customer, Integer> o1,  
                    Map.Entry<Customer, Integer> o2) {  
                return (o1.getValue()).toString().compareTo(o2.getValue().toString());
                
            }
        });
        
        int n =0;
        for(int i=customerMoneyList.size()-1;i>=0;i--){
            if(n<3){
                n++;
                System.out.println("Top "+n+" customer is "+customerMoneyList.get(i).getKey().toString()+",and he spent "+customerMoneyList.get(i).getValue()+".");
                continue;
            }else if(n==3){
                if(Objects.equals(customerMoneyList.get(i).getValue(), customerMoneyList.get(i+1).getValue())){
                   System.out.println("Top "+n+" customer is "+customerMoneyList.get(i).getKey().toString()+",and he spent "+customerMoneyList.get(i).getValue()+".");
                }
                else break;
            }
            
        }
        System.out.println("-------------------------------------------------------------");
        
        
    }
    
        public void top3Sales(){
            System.out.println("The top 3 sales person is:(By revenue)");
            Map<SalesPerson,Integer> sales = new HashMap<SalesPerson,Integer>();
            Map<Integer,SalesPerson> salesMap=DataStore.getInstance().getSalesPerson();
            for(SalesPerson salesMan:salesMap.values()){
                int money=0;
                for(Order order:salesMan.getOrders()){
                    money += order.getItem().getQuantity()*(order.getItem().getSalesPrice());
                }
                sales.put(salesMan, money);
            }
        
        List<Map.Entry<SalesPerson,Integer>> salesList =new ArrayList<Map.Entry<SalesPerson,Integer>>(sales.entrySet());
        Collections.sort(salesList,new Comparator<Map.Entry<SalesPerson,Integer>>(){
            public int compare(Map.Entry<SalesPerson, Integer> o1,  
                    Map.Entry<SalesPerson, Integer> o2) {  
                return (o1.getValue()).toString().compareTo(o2.getValue().toString());
                
            }
        });
        
        int n =0;
        for(int i=salesList.size()-1;i>=0;i--){
            if(n<3){
                n++;
                System.out.println("Top "+n+" sales person is "+salesList.get(i).getKey().toString()+",and he sold "+salesList.get(i).getValue()+".");
                continue;
            }else if(n==3){
                if(Objects.equals(salesList.get(i).getValue(), salesList.get(i+1).getValue())){
                   System.out.println("Top "+n+" sales person is "+salesList.get(i).getKey().toString()+",and he sold "+salesList.get(i).getValue()+".");
                }
                else break;
            }
            
        }
        System.out.println("-------------------------------------------------------------");
        
        
    }
        
        public void top3SalesProfit(){
            System.out.println("The top 3 sales person is:(By profit)");
            Map<SalesPerson,Integer> sales = new HashMap<SalesPerson,Integer>();
            Map<Integer,Product> products = DataStore.getInstance().getProduct();
            Map<Integer,SalesPerson> salesMap=DataStore.getInstance().getSalesPerson();
            for(SalesPerson salesMan:salesMap.values()){
                int money=0;
                for(Order order:salesMan.getOrders()){
                    money += (order.getItem().getQuantity()*((order.getItem().getSalesPrice())-products.get(order.getItem().getProductId()).getMinPrize()));
                }
                sales.put(salesMan, money);
            }
        
        List<Map.Entry<SalesPerson,Integer>> salesList =new ArrayList<Map.Entry<SalesPerson,Integer>>(sales.entrySet());
        Collections.sort(salesList,new Comparator<Map.Entry<SalesPerson,Integer>>(){
            public int compare(Map.Entry<SalesPerson, Integer> o1,  
                    Map.Entry<SalesPerson, Integer> o2) {  
                return (o1.getValue()).toString().compareTo(o2.getValue().toString());
                
            }
        });
        
        int n =0;
        for(int i=salesList.size()-1;i>=0;i--){
            if(n<3){
                n++;
                System.out.println("Top "+n+" sales person is "+salesList.get(i).getKey().toString()+",and he sold "+salesList.get(i).getValue()+".");
                continue;
            }else if(n==3){
                if(Objects.equals(salesList.get(i).getValue(), salesList.get(i+1).getValue())){
                   System.out.println("Top "+n+" sales person is "+salesList.get(i).getKey().toString()+",and he sold "+salesList.get(i).getValue()+".");
                }
                else break;
            }
            
        }
        System.out.println("-------------------------------------------------------------");
        
        
    }
    
    public void revenue(){
         System.out.print("The total revenue is: ");
         Map<Integer,Integer> productQuantity =new HashMap<>();
         Map<Integer,Product> products = DataStore.getInstance().getProduct();
         int revenue=0;
         int p=0;
         for(Product product:products.values()){
             for(Order order:product.getOrders()){
            revenue += order.getItem().getQuantity()*order.getItem().getSalesPrice();
            p += (order.getItem().getQuantity()*order.getItem().getSalesPrice()-(order.getItem().getQuantity()*product.getMinPrize()));
             }
         }
         
         System.out.println(revenue+".");
         System.out.println("The profit of this year is: "+p+".");
         
    }
}
