/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author harshalneelkamal
 */
public class Product {
    
    private int productId;
    private int minPrize;
    private int maxPrize;
    private int targetPrize;
    private List<Order> orders;

    public Product(int productId, int minPrize, int maxPrize, int targetPrize) {
        this.productId = productId;
        this.minPrize = minPrize;
        this.maxPrize = maxPrize;
        this.targetPrize = targetPrize;
        this.orders = new ArrayList<>();
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getMinPrize() {
        return minPrize;
    }

    public void setMinPrize(int minPrize) {
        this.minPrize = minPrize;
    }

    public int getMaxPrize() {
        return maxPrize;
    }

    public void setMaxPrize(int maxPrize) {
        this.maxPrize = maxPrize;
    }

    public int getTargetPrize() {
        return targetPrize;
    }

    public void setTargetPrize(int targetPrize) {
        this.targetPrize = targetPrize;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
