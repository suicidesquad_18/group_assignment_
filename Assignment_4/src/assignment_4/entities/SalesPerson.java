/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author harshalneelkamal
 */
public class SalesPerson {
    private int salesId;
    private String firstName;
    private String lastName;
    private List<Order> orders;

    public SalesPerson(int salesId, String firstName, String lastName) {
        this.salesId = salesId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.orders = new ArrayList<>();
    }

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
    
    @Override
    public String toString() {
        return "Sales{" + "id = " + salesId + ", firstName = " + firstName + ", lastName = " + lastName + ", no. of orders = " + orders.size() + '}';
    }
}
