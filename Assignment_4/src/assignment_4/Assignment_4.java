/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.io.IOException;
import java.util.Map;
import assignment_4.analytics.AnalysisHelper;

/**
 *
 * @author machao
 */
public class Assignment_4 {

    DataReader orderReader;
    DataReader productReader;
    AnalysisHelper helper;
    
    public Assignment_4() throws IOException {
        DataGenerator generator = DataGenerator.getInstance();
        orderReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
        // init helper
        helper=new AnalysisHelper();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {     
        Assignment_4 inst = new Assignment_4();
        inst.readData();
    }
    
    private void readData() throws IOException{
        String[] row;
        while((row = productReader.getNextRow()) != null ){
            generateProduct(row);
        }
        while((row = orderReader.getNextRow()) != null ){
            Item item = generateItem(row);
            generateCustomer(row);
            generateSalesPerson(row);
            generateOrder(row, item);
        }
        try {
            runAnalysis();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void generateProduct(String[] row){
        int productId = Integer.parseInt(row[0]);
        int minPrize = Integer.parseInt(row[1]);
        int maxPrize = Integer.parseInt(row[2]);
        int targetPrize = Integer.parseInt(row[3]);
        Product p = new Product(productId, minPrize, maxPrize, targetPrize);
        DataStore.getInstance().getProduct().put(productId, p);
    }
    
    private Item generateItem(String[] row){
        int itemId = Integer.parseInt(row[1]);
        int productId = Integer.parseInt(row[2]);
        int salesId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        int salesPrice = Integer.parseInt(row[6]);
        int quantity = Integer.parseInt(row[3]);
        Item item = new Item(itemId, productId, salesPrice, quantity);
        DataStore.getInstance().getItem().put(itemId, item);
        return item;
    }
    
    private void generateOrder(String[] row, Item item){
        int orderId = Integer.parseInt(row[0]);
        int productId = Integer.parseInt(row[2]);
        int customerId = Integer.parseInt(row[5]);
        int salesId = Integer.parseInt(row[4]);
        Order order = new Order(orderId, salesId, customerId, item);
        DataStore.getInstance().getOrder().put(orderId, order);
        // add in order list of customer class
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomer();
        if(customers.containsKey(customerId)) {
            customers.get(customerId).getOrders().add(order);
        }
        // add in order list of SalesPerson class
        Map<Integer, SalesPerson> sales = DataStore.getInstance().getSalesPerson();
        if(sales.containsKey(salesId)) {
            sales.get(salesId).getOrders().add(order);
        }
        // add in order list of Product class
        Map<Integer, Product> products = DataStore.getInstance().getProduct();
        if(products.containsKey(productId)) {
            products.get(productId).getOrders().add(order);
        }
    }
    
    private void generateCustomer(String[] row){
        int customerId = Integer.parseInt(row[5]);
        Customer c = new Customer(customerId, "Customer FirstName " + customerId, "Customer LastName " + customerId);
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomer();
        // dedup
        if (!customers.containsKey(customerId)) {
            DataStore.getInstance().getCustomer().put(customerId, c);
        }
    }
    
    private void generateSalesPerson(String[] row){
        int salesId = Integer.parseInt(row[4]);
        SalesPerson s = new SalesPerson(salesId, "Sales FirstName " + salesId, "Sales LastName " + salesId);
        Map<Integer, SalesPerson> salesPersons = DataStore.getInstance().getSalesPerson();
        // dedup
        if (!salesPersons.containsKey(salesId)) {
            DataStore.getInstance().getSalesPerson().put(salesId, s);
        }  
    }
    
    private void runAnalysis(){
        helper.top3PopularProduct();
        helper.top3Customer();
        helper.top3Sales();
        helper.top3SalesProfit();;
        helper.revenue();
    }
}
