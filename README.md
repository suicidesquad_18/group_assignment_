# Team Name:
Suicide Squad 

# Team memember:
Qianjin Wu,
Zhouwei Wang,
Chao Ma


# Group_Assignment_1

## Requirements:
Start with the Skeleton provided to you for lab 5.
You need to work in a group of 3.
Extend Customer class provided in the skeleton from User class.
Implement the abstract method in Customer class.
Add validation to the "AdminCreateScreen". 
Use Regex pattern matching to verify Username and Password.
Password: should contain alphanumeric characters with "+_$" being the allowed special characters.
Username: should be an email-ID with "_" and "@" as the only allowed special characters but should not start with an "_".
Complete the customer login screen.
u also need to populate the Customer Table !! and ***** in the login screen should be replaced by text "Supplier" or "Customer" based on who is trying to log in. 

## How to run:
1. First clone this repository in your local repository.
2. Then open and run it in netbeans.
3. In the JFrame, click on Admin button where you can add a User which is either a supplier or a customer.
4. Then, after clicking on the customer or supplier button, you can choose the user you just created and enter the password to login.
5. The success panel will be shown if successfully logged in.
6.In terms of username validation, the user name has to be an email format like *@*.*, and in each "*" users 
are allowed to have upper and lowercase letters and numbers as well as underscore. For the first two "*",
they can be repeated any times and for the last"*", it can only be repeated 2-5 times.And also the username 
is not allowed to start with an underscore.


# Assignment 4:

## Requirements:
The objective of this assignment is to learn how to implement and populate complex commerce and trade models. In this exercise, implement the Xerox sales process object model by map it to java classes and then populate the model with products, customer, orders, and sales personnel. In addition, you are required to read data from a file that have product, customer, and order data. Once the data is loaded to the model, you are required to aggregate the data and output to a report in the form of a dashboard. Your report must answer the following questions and all in relation revenue totals:
Our top 3 most popular product sorted from high to low.
Our 3 best customers 
Our top 3 best sales people
Our total revenue for the year

## How to run:
1. First clone this repository in your local repository.
2. Then open and run it in netbeans.
3. The results will display in the output window below.

# Final Project:

## Object Model:
https://bitbucket.org/suicidesquad_18/group_assignment_/src/master/SystemDiagram.svg

## Slides:
https://bitbucket.org/suicidesquad_18/group_assignment_/src/master/MSA%20PLATFORM%20(DEMO)/5100%20Final%20Project%20-%20Suicide%20Squad%20(1).pptx

## How to run:
1. First clone this repository in your local repository.
2. Then open MSA Platform and run it in netbeans.
3. Use sysadmin as username and password to login or register new user.
4. Enjoy.


