/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Forum;

import Business.UserAccount.UserAccount;
import java.util.Date;

/**
 *
 * @author machao
 */
public class Post {
    private String title;
    private String content;
    private UserAccount sender;
    private String status;
    private Date postDate;
    private Date resolveDate;
    private CommentDirectory commentDir;

    public Post() {
        this.postDate = new Date();
        this.commentDir = new CommentDirectory();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }

    public CommentDirectory getCommentDir() {
        return commentDir;
    }
    
}
