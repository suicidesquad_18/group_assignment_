/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Forum;

import Business.UserAccount.UserAccount;
import java.util.ArrayList;

/**
 *
 * @author machao
 */
public class PostDirectory {
    private ArrayList<Post> postList;   

    public PostDirectory() {
        postList = new ArrayList<Post>();
    }

    public ArrayList<Post> getPostList() {
        return postList;
    }

    public void setPostList(ArrayList<Post> postList) {
        this.postList = postList;
    }
    
    //Create post
    public Post createAndAddPost(String title, String content, UserAccount sender){
            Post post = new Post();
            post.setTitle(title);
            post.setContent(content);
            post.setSender(sender);
            postList.add(post);
        return post;   
    }
}
