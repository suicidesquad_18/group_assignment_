/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Forum;

import Business.UserAccount.UserAccount;
import java.util.ArrayList;

/**
 *
 * @author machao
 */
public class CommentDirectory {
    private ArrayList<Comment> commentList;

    public CommentDirectory() {
        commentList = new ArrayList<Comment>();
    }

    public ArrayList<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(ArrayList<Comment> commentList) {
        this.commentList = commentList;
    }
    
    //Create post
    public Comment createAndAddCommnet(String content, UserAccount sender){
            Comment comment = new Comment();
            comment.setContent(content);
            comment.setSender(sender);
            commentList.add(comment);
        return comment;
    }
}
