/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author wu
 */
public class WorkQueue {
    
    private ArrayList<WorkRequest> workRequestList;

    public WorkQueue() {
        workRequestList = new ArrayList();
    }

    public ArrayList<WorkRequest> getWorkRequestList() {
        return workRequestList;
    }
    
    public ArrayList<WorkRequest> searchRelationship(ArrayList<WorkRequest> requestList, Integer n){
        ArrayList<WorkRequest> completedList = new ArrayList<WorkRequest>();
        for(WorkRequest request: requestList){
            if(request.getRelationship()!= null){
                if(request.getRelationship().equals(n))
                completedList.add(request);
                
            }
        }
        return completedList;
    }

    
    public ArrayList<WorkRequest> searchCompleted(ArrayList<WorkRequest> requestList){
        ArrayList<WorkRequest> completedList = new ArrayList<WorkRequest>();
        for(WorkRequest request: requestList){
            if((request.getStatus().equals("Completed")||request.getStatus().equals("Cancelled"))){
                completedList.add(request);
            }
        }
        return completedList;
    }
    
    public ArrayList<WorkRequest> searchWaiting(ArrayList<WorkRequest> requestList){
        ArrayList<WorkRequest> waitingList = new ArrayList<WorkRequest>();
        for(WorkRequest request: requestList){
            if(request.getStatus().equals("Sent")){
                waitingList.add(request);
            }
        }
        return waitingList;
    }
}
