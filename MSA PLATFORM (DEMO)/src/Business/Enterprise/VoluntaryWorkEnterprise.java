/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Roles.Roles;
import java.util.ArrayList;

/**
 *
 * @author wu
 */
public class VoluntaryWorkEnterprise extends Enterprise {
    public VoluntaryWorkEnterprise(String name){
        super(name,Enterprise.EnterpriseType.VoluntaryWorkCenter);
    }
    @Override
    public ArrayList<Roles> getSupportedRole() {
        return null;
    }
    
}
