/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.OrganizationHospital.OrganizationHospital;
import Business.OrganizationHospital.OrganizationHospitalDirectory;

/**
 *
 * @author wu
 */
public abstract class Enterprise extends OrganizationHospital{
    
    private EnterpriseType enterpriseType;
    private OrganizationHospitalDirectory organizationHospitalDirectory;

    public OrganizationHospitalDirectory getOrganizationHospitalDirectory() {
        return organizationHospitalDirectory;
    }
    
    public enum EnterpriseType{
        Hospital("Hospital"),
        VoluntaryWorkCenter("MSAVoluntaryWorkCenter");
        
        private String value;
        
        private EnterpriseType(String value){
            this.value=value;
        }
        public String getValue() {
            return value;
        }
        @Override
        public String toString(){
        return value;
    }
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }
    
    public Enterprise(String name,EnterpriseType type){
        super(name);
        this.enterpriseType=type;
        organizationHospitalDirectory=new OrganizationHospitalDirectory();
    }
}
