/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.OrganizationVolunteer;


import Business.OrganizationHospital.OrganizationHospital;
import Business.Roles.AdminsHos;
import Business.Roles.Roles;
import java.util.ArrayList;


/**
 *
 * @author wu
 */
public class AdminOrg extends OrganizationHospital{
    public AdminOrg(){
        super(OrganizationHospital.Type.AdminVol.getValue());
    
    }
    @Override
    public ArrayList<Roles> getSupportedRole() {
        ArrayList<Roles> roles = new ArrayList();
        roles.add(new AdminsHos());
        return roles;
    }
    
}
