/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.OrganizationVolunteer;

import Business.Employee.EmployeeDirectory;
import Business.Roles.Roles;
import Business.UserAccount.UserAccountDirectory;
import Business.Volunteer.VolDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author wu
 */
 public abstract class OrganizationVolunteer {
    private String name;
    private WorkQueue workQueue;
    private VolDirectory volDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter=0;
    public enum Type{
        AdminVol("Admin Organization"), Volunteer("Volunteer Community");
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
    public OrganizationVolunteer(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        volDirectory = new VolDirectory();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        ++counter;
    }

    public WorkQueue getWorkQueueVtoP() {
        return workQueue;
    }

    public void setWorkQueueVtoP(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }
    public abstract ArrayList<Roles> getSupportedRole();
   
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    

    public VolDirectory getVolDirectory() {
        return volDirectory;
    }

    public void setVolDirectory(VolDirectory volDirectory) {
        this.volDirectory = volDirectory;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(int organizationID) {
        this.organizationID = organizationID;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        OrganizationVolunteer.counter = counter;
    }
    
    
    @Override
    public String toString() {
        return name;
    }
    
    
}
