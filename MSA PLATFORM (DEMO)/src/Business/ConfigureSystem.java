/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Volunteer.Volunteers;
import Business.Roles.SystemAdmin;
import Business.UserAccount.UserAccount;

/**
 *
 * @author wu
 */
public class ConfigureSystem {
    
    
    public static MSAPlatform configure(){
        MSAPlatform platform = MSAPlatform.getInstance();
        
        Employee employee = platform.getEmployeeDirectory().createEmployee("XX");
        Volunteers volunteer = platform.getVolDirectory().createVolunteer("XXX");
        
        UserAccount ua = platform.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", volunteer, employee, new SystemAdmin());
        
        return platform;
    }
    
}
