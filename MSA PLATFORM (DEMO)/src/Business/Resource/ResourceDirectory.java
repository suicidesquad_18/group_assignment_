/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Resource;

import Business.UserAccount.UserAccount;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author machao
 */
public class ResourceDirectory {
    private ArrayList<Resource> resourceList;

    public ResourceDirectory() {
        resourceList = new ArrayList<>();
    }

    public ArrayList<Resource> getResourceList() {
        return resourceList;
    }

    public Resource createResource(File file, String fileName, UserAccount sender){
        Resource resource = new Resource(file, fileName, sender);
        resourceList.add(resource);
        return resource;
    }
}
