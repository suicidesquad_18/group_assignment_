/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Resource;

import Business.UserAccount.UserAccount;
import java.io.File;

/**
 *
 * @author machao
 */
public class Resource {
    private File file;
    private String fileName;
    private UserAccount sender;
    private int views;

    public Resource(File file, String fileName, UserAccount sender) {
        this.file = file;
        this.fileName = fileName;
        this.sender = sender;
        this.views = 0;
    }

    public File getFile() {
        return file;
    }

    public String getFileName() {
        return fileName;
    }

    public UserAccount getSender() {
        return sender;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }
    
}
