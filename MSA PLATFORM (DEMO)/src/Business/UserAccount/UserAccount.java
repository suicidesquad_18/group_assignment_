/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Employee.Employee;
import Business.Roles.Roles;
import Business.Volunteer.Volunteers;
import Business.WorkQueue.WorkQueue;


/**
 *
 * @author wu
 */
public class UserAccount {
    private String username;
    private String password;
    private Employee employee;
    private Volunteers volunteer;
    private Roles role;
    private WorkQueue workQueue;
    
    public UserAccount(){
        workQueue = new WorkQueue();
        
    }

    public Volunteers getVolunteer() {
        return volunteer;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setVolunteer(Volunteers volunteer) {
        this.volunteer = volunteer;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }
    
    @Override
    public String toString(){
        return username;
    }
    
    
    
    
}
