/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Patients;

import java.util.ArrayList;

/**
 *
 * @author littlelion
 */
public class DailyRecordList {
    
    private ArrayList<DailyRecord> dailyRecordList;
    
    public DailyRecordList(){
        dailyRecordList =new ArrayList<DailyRecord>();
    }

    public ArrayList<DailyRecord> getDailyRecordList() {
        return dailyRecordList;
    }

    public void setDailyRecordList(ArrayList<DailyRecord> dailyRecordList) {
        this.dailyRecordList = dailyRecordList;
    }
    
    
}
