/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Roles;

import Business.Enterprise.Enterprise;
import Business.MSAPlatform;
import Business.OrganizationHospital.OrganizationHospital;
import Business.OrganizationVolunteer.OrganizationVolunteer;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author wu
 */
public abstract class Roles {
    public enum roleTypes{
        Admin("Admin"),
        Doctor("Doctor"),
        Researcher("Researcher"),
        Patient("Patient"),
        Volunteer("Volunteers");
        private String value;
        private roleTypes(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    
    
    
    }
    public abstract JPanel createWorkArea(JPanel rightJPanel, 
            UserAccount account, 
            OrganizationHospital organizationHospital, 
            OrganizationVolunteer organizationVolunteer,
            Enterprise enterprise, 
            MSAPlatform business);
    
    @Override
    public String toString() {
        return this.getClass().getName();
    }

    
}
