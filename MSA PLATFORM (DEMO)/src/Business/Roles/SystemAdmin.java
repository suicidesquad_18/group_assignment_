/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Roles;

import Business.Enterprise.Enterprise;
import Business.MSAPlatform;
import Business.OrganizationHospital.OrganizationHospital;
import Business.OrganizationVolunteer.OrganizationVolunteer;
import Business.UserAccount.UserAccount;
import Userinterface.SystemAdmin.SystemAdminWorkArea;
import javax.swing.JPanel;

/**
 *
 * @author wu
 */
public class SystemAdmin extends Roles{

    @Override
    public JPanel createWorkArea(JPanel rightJPanel, UserAccount account, OrganizationHospital organizationHospital,OrganizationVolunteer organizationVolunteer, Enterprise enterprise, MSAPlatform platform) {
        return new SystemAdminWorkArea(rightJPanel, platform);
    }
    
}