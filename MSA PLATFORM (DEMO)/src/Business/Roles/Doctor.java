/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Roles;

import Business.Enterprise.Enterprise;
import Business.MSAPlatform;
import Business.OrganizationHospital.OrganizationHospital;
import Business.OrganizationVolunteer.OrganizationVolunteer;
import Business.UserAccount.UserAccount;
import Userinterface.DoctorRole.DoctorWorkArea;
import javax.swing.JPanel;

/**
 *
 * @author wu
 */
public class Doctor extends Roles{
    
    private String license;
    private String birth;
    private String selfIntro;
    private String gender;

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getSelfIntro() {
        return selfIntro;
    }

    public void setSelfIntro(String selfIntro) {
        this.selfIntro = selfIntro;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    

    @Override
    public JPanel createWorkArea(JPanel rightJPanel, UserAccount account, OrganizationHospital organizationHospital, OrganizationVolunteer organizatioVolunteer, Enterprise enterprise, MSAPlatform platform) {
        return new DoctorWorkArea(rightJPanel, account, organizationHospital, enterprise);
    }
    
    
}

