/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Roles;

import Business.Enterprise.Enterprise;
import Business.MSAPlatform;
import Business.OrganizationHospital.OrganizationHospital;
import Business.OrganizationVolunteer.OrganizationVolunteer;
import Business.OrganizationVolunteer.VolunteerOrg;
import Business.UserAccount.UserAccount;
import Userinterface.VolunteerRole.VolunteerWorkArea;
import javax.swing.JPanel;

/**
 *
 * @author wu
 */
public class Volunteer extends Roles{
    
    private String birth;
    private String gender;
    private String selfIntro;

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSelfIntro() {
        return selfIntro;
    }

    public void setSelfIntro(String selfIntro) {
        this.selfIntro = selfIntro;
    }
    
    
    
    
    @Override
    public JPanel createWorkArea(JPanel rightJPanel, UserAccount account, OrganizationHospital organizationHospital, OrganizationVolunteer organizationVolunteer,Enterprise enterprise, MSAPlatform platform) {
        // return new VolunteerWorkArea(rightJPanel, account, organizationHospital, platform);
        return new VolunteerWorkArea(rightJPanel,account,organizationHospital,organizationVolunteer,enterprise,platform);
    }
    
}
