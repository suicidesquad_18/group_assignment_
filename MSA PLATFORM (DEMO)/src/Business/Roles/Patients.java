/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Roles;

import Business.Enterprise.Enterprise;
import Business.MSAPlatform;
import Business.OrganizationHospital.OrganizationHospital;
import Business.OrganizationVolunteer.OrganizationVolunteer;
import Business.Patients.DailyRecord;
import Business.Patients.DailyRecordList;
import Business.UserAccount.UserAccount;

import Userinterface.PatientRole.PatientWorkArea;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author wu
 */
public class Patients extends Roles{
    private DailyRecordList record;
    
    private String birth;
    private String gender;
    private String selfIntro;

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSelfIntro() {
        return selfIntro;
    }

    public void setSelfIntro(String selfIntro) {
        this.selfIntro = selfIntro;
    }
    
    
    
    public Patients(){
    record = new DailyRecordList();
    }

    public DailyRecordList getRecord() {
        return record;
    }

    public void setRecord(DailyRecordList record) {
        this.record = record;
    }
    
    
    
    @Override
    public JPanel createWorkArea(JPanel rightJPanel, UserAccount account, OrganizationHospital organizationHospital,OrganizationVolunteer organizationVolunteer, Enterprise enterprise, MSAPlatform platform) {
        return new PatientWorkArea(rightJPanel, account, enterprise, platform);
    }
    
}
