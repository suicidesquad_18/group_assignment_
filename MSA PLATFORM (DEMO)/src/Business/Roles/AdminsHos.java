/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Roles;

import Business.Enterprise.Enterprise;
import Business.MSAPlatform;
import Business.OrganizationHospital.OrganizationHospital;
import Business.OrganizationVolunteer.OrganizationVolunteer;
import Business.UserAccount.UserAccount;
import Userinterface.EnterpriseAdmin.AdminWorkArea;
import javax.swing.JPanel;

/**
 *
 * @author wu
 */
public class AdminsHos extends Roles{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, OrganizationHospital organizationHospital, OrganizationVolunteer organizationVolunteer, Enterprise enterprise, MSAPlatform platform) {
        return new AdminWorkArea(userProcessContainer, enterprise,platform);
    }

    
    
}
