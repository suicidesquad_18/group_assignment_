/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.OrganizationHospital;

import Business.Roles.AdminsHos;
import Business.Roles.Roles;
import java.util.ArrayList;


/**
 *
 * @author wu
 */
public class AdminHosOrg extends OrganizationHospital{
    public AdminHosOrg(){
        super(Type.AdminHos.getValue());
    
    }
    @Override
    public ArrayList<Roles> getSupportedRole() {
        ArrayList<Roles> roles = new ArrayList();
        roles.add(new AdminsHos());
        return roles;
    }
    
}
