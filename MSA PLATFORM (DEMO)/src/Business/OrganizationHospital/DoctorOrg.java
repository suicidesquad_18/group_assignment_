/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.OrganizationHospital;

import Business.Roles.Doctor;
import Business.Roles.Roles;
import java.util.ArrayList;

/**
 *
 * @author wu
 */
public class DoctorOrg extends OrganizationHospital{
    public DoctorOrg() {
        super(OrganizationHospital.Type.Doctor.getValue());
    }
    @Override
    public ArrayList<Roles> getSupportedRole() {
        ArrayList<Roles> roles = new ArrayList();
        roles.add(new Doctor());
        return roles;
    }
     
    
    
}

