/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.OrganizationHospital;

import Business.OrganizationHospital.OrganizationHospital.Type;
import Business.OrganizationVolunteer.VolunteerOrg;
import java.util.ArrayList;


/**
 *
 * @author wu
 */
public class OrganizationHospitalDirectory {
    private ArrayList<OrganizationHospital> organizationHospitalList;

    public OrganizationHospitalDirectory() {
        organizationHospitalList = new ArrayList();
    }

    public ArrayList<OrganizationHospital> getOrganizationHospitalList() {
        return organizationHospitalList;
    }

    
    
    public OrganizationHospital createOrganization(Type type){
        OrganizationHospital organizationHos = null;
        if (type.getValue().equals(Type.Doctor.getValue())){
            organizationHos = new DoctorOrg();
            organizationHospitalList.add(organizationHos);
        }
        else if (type.getValue().equals(Type.ResearchInstitution.getValue())){
            organizationHos = new ResearchInstitution();
            organizationHospitalList.add(organizationHos);
        }
        else if(type.getValue().equals(Type.Volunteer.getValue())){
            organizationHos =new VolunteerOrg();
            organizationHospitalList.add(organizationHos);
        }
        else if(type.getValue().equals(Type.Patients.getValue())){
            organizationHos =new PatientsCommunity();
            organizationHospitalList.add(organizationHos);
        }
        return organizationHos;
    }
    
}
