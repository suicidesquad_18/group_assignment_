/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.OrganizationHospital;

import Business.Employee.EmployeeDirectory;
import Business.Roles.Roles;
import Business.UserAccount.UserAccountDirectory;
import Business.Volunteer.VolDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;


/**
 *
 * @author wu
 */
public abstract class OrganizationHospital {
    private String name;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDirectory;
    private VolDirectory volDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter=0;
    
    
    public enum Type{
        AdminHos("Admin Organization"), Volunteer("Volunteer Community"),AdminVol("Admin Organization"), Doctor("Doctor Organization"), ResearchInstitution("Research Institution"), Patients("Patients Community");
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public OrganizationHospital(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        volDirectory = new VolDirectory();
        organizationID = counter;
        ++counter;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueueDtoR(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public abstract ArrayList<Roles> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public String getName() {
        return name;
    }

    

    public void setName(String name) {
        this.name = name;
    }

    public VolDirectory getVolDirectory() {
        return volDirectory;
    }

    public void setVolDirectory(VolDirectory volDirectory) {
        this.volDirectory = volDirectory;
    }

    @Override
    public String toString() {
        return name;
    }

    
    
    
}
