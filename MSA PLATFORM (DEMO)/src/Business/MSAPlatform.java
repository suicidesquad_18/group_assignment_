/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Forum.PostDirectory;
import Business.Location.Location;
import Business.OrganizationHospital.OrganizationHospital;
import Business.Roles.Roles;
import Business.Roles.SystemAdmin;
import java.util.ArrayList;
import Business.OrganizationHospital.OrganizationHospitalDirectory;
import Business.Resource.ResourceDirectory;
import Business.UserAccount.UserAccount;


/**
 *
 * @author wu
 */
public class MSAPlatform extends OrganizationHospital {

      
    private static MSAPlatform business;
    private ArrayList<Location> locationList;
    private PostDirectory postDirectory;
    private OrganizationHospital defaultOrganization;
    private Employee registrationManager;
    private ResourceDirectory resourceDirectory;
    
    public static MSAPlatform getInstance(){
        if(business == null){
            business = new MSAPlatform();
        }
        return business;
    }
    
    public Location createAndAddLocation(){
        Location location = new Location();
        locationList.add(location);
        return location;
    }
    
    @Override
    public ArrayList<Roles> getSupportedRole() {
        ArrayList<Roles> roleList = new ArrayList<Roles>();
        roleList.add(new SystemAdmin());
        return roleList;
    }
    
    private MSAPlatform(){
        super(null);
        locationList = new ArrayList<Location>();
        System.out.println("MSAPlatform");
        postDirectory = new PostDirectory();
        resourceDirectory = new ResourceDirectory();
        
        createDefaultOrg();
    }
    
    private void createDefaultOrg() {
        Location location = this.createAndAddLocation();
        location.setName("Head Quarter");
        
        Enterprise enterprise = location.getEnterpriseDirectory().createAndAddEnterprise("Patient Center", Enterprise.EnterpriseType.Hospital);
        OrganizationHospitalDirectory orgDir = enterprise.getOrganizationHospitalDirectory();
        OrganizationHospital org = orgDir.createOrganization(Type.Patients);
        this.defaultOrganization = org;
        this.registrationManager = org.getEmployeeDirectory().createEmployee("Registration Manager");
    }

    public ArrayList<Location> getLocationList() {
        return locationList;
    }

    public void setLocationList(ArrayList<Location> locationList) {
        this.locationList = locationList;
    }

    public PostDirectory getPostDirectory() {
        return postDirectory;
    }

    public boolean checkIfUserIsUnique(String userName){
        return this.getUserAccountDirectory().checkIfUsernameIsUnique(userName);
    } 

    public OrganizationHospital getDefaultOrganization() {
        return defaultOrganization;
    }

    public Employee getRegistrationManager() {
        return registrationManager;
    }

    public ResourceDirectory getResourceDirectory() {
        return resourceDirectory;
    }
    
    public boolean repeated(String name){
        for(Location l: locationList){
            System.out.println(l.getName());
            for(Enterprise e: l.getEnterpriseDirectory().getEnterpriseList()){
                for(OrganizationHospital org:e.getOrganizationHospitalDirectory().getOrganizationHospitalList()){
                    for(UserAccount acc:org.getUserAccountDirectory().getUserAccountList()){
                        if(acc.getUsername().equals(name)||name.equals("sysadmin")){
                            return true;
                            
                        }
                    }
                }
            }
        }
        return false;

    }
    
}
