/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Userinterface.PatientRole;

import Business.Enterprise.Enterprise;
import Business.Enterprise.HospitalEnterprise;
import Business.Location.Location;
import Business.MSAPlatform;
import Business.OrganizationHospital.OrganizationHospital;
import Business.OrganizationHospital.PatientsCommunity;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.AdditionalRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author littlelion
 */
public class ToVoluneer_MyRequest extends javax.swing.JPanel {

    /**
     * Creates new form ToVoluneer_MyRequest
     */
    private JPanel userProcessContainer;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private MSAPlatform system;
    public ToVoluneer_MyRequest(JPanel userProcessContainer, UserAccount userAccount, Enterprise enterprise, MSAPlatform system) {
        initComponents();
        
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        this.system = system;
        
        populatePatientRequestHistory();
        populateVolunteer();
        addComboBox();
        populateComboBoxVolunteer();
    }
    
    private void addComboBox(){
        this.ComboBoxType.removeAllItems();
        this.ComboBoxType.addItem("");
        this.ComboBoxType.addItem("Movement");
        this.ComboBoxType.addItem("Caring");
        this.ComboBoxType.addItem("Washing");
        this.ComboBoxType.addItem("Eating");
        this.ComboBoxType.addItem("Others");
    }
    
    public void populatePatientRequestHistory(){
        
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        
        model.setRowCount(0);
        for (WorkRequest request :userAccount.getWorkQueue().searchRelationship(userAccount.getWorkQueue().getWorkRequestList(),1)){
            request.getSender().getUsername();
            Object[] row = new Object[5];
            
            row[0]=((AdditionalRequest)request).getReservationTime();
            row[1]=((AdditionalRequest)request).getWorkType();
            
            //row[2]=request.getReceiver()==null?"Waiting":request.getReceiver().getUsername();
            if(request.getReceiver()!=null){
                if(request.getReceiver().equals(userAccount)){
                    row[2]=request.getSender().getUsername();
                }else row[2]=request.getReceiver().getUsername();
            }else row[2]="Waiting";
            row[3]=request.getStatus();
            if(((AdditionalRequest)request).isContact()==true){
                row[3]=request.getStatus()+"(New message!!)";
            }
            row[4]=request;
            
            model.addRow(row);
            
            
            
           
        }
        
        
        
        
    }
    
    public void populateVolunteer(){
        DefaultTableModel model = (DefaultTableModel) TblVolunteer.getModel();
        
        model.setRowCount(0);
        
        
            
                    for (OrganizationHospital organization : enterprise.getOrganizationHospitalDirectory().getOrganizationHospitalList()){
                        if (organization instanceof PatientsCommunity){
                            for (WorkRequest request :organization.getWorkQueue().searchWaiting(organization.getWorkQueue().getWorkRequestList())){

                                
                                Object[] row = new Object[4];

                                row[0]=request.getSender().getUsername();
                                row[1]=((AdditionalRequest)request).getReservationTime();
                                row[2]=((AdditionalRequest)request).getWorkType();
                                row[3]=request;
                                model.addRow(row);
                            }
                        }
                    
                
            
                    }
       
        


    }
    
    public void populateComboBoxVolunteer(){
            this.ComboBoxName.removeAllItems();
            //JComboBox ComboBoxName= new JComboBox();
            //ComboBoxName.setModel( new DefaultComboBoxModel());
            ArrayList<UserAccount> v =new ArrayList<UserAccount>();
            for (OrganizationHospital organization : enterprise.getOrganizationHospitalDirectory().getOrganizationHospitalList()){
                        if (organization instanceof PatientsCommunity){
                            for (WorkRequest request :organization.getWorkQueue().searchWaiting(organization.getWorkQueue().getWorkRequestList())){
                                if(!v.contains(request.getSender())){
                                    v.add(request.getSender());
                                }
                            }
                        }
            }
            
            ComboBoxName.addItem("");
            for(UserAccount acc:v){
                System.out.println(acc);
                ComboBoxName.addItem(acc.getUsername());
                    
            }
            
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        BtnComment = new javax.swing.JButton();
        BtnFinished = new javax.swing.JButton();
        BtnCancel = new javax.swing.JButton();
        BtnBack = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        ComboBoxName = new javax.swing.JComboBox<>();
        TxtTime = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        TblVolunteer = new javax.swing.JTable();
        ComboBoxType = new javax.swing.JComboBox<>();
        BtnView = new javax.swing.JButton();
        BtnSearch = new javax.swing.JButton();
        BtnChoose = new javax.swing.JButton();
        BtnNewRequest = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        BtnContact = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("My Help Request");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Time", "WorkType", "Volunteer", "Status", "request"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(4).setMinWidth(0);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(0);
            jTable1.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        BtnComment.setText("Comment");
        BtnComment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCommentActionPerformed(evt);
            }
        });

        BtnFinished.setText("Finished");
        BtnFinished.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFinishedActionPerformed(evt);
            }
        });

        BtnCancel.setText("Cancel");
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });

        BtnBack.setText("Back");
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });

        jLabel5.setText("WorkType:");

        ComboBoxName.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        TblVolunteer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "VolunteerName", "AvailableTime", "WorkType", "request"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(TblVolunteer);
        if (TblVolunteer.getColumnModel().getColumnCount() > 0) {
            TblVolunteer.getColumnModel().getColumn(3).setMinWidth(0);
            TblVolunteer.getColumnModel().getColumn(3).setPreferredWidth(0);
            TblVolunteer.getColumnModel().getColumn(3).setMaxWidth(0);
        }

        ComboBoxType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        BtnView.setText("View Details");
        BtnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnViewActionPerformed(evt);
            }
        });

        BtnSearch.setText("Search");
        BtnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSearchActionPerformed(evt);
            }
        });

        BtnChoose.setText("Choose");
        BtnChoose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnChooseActionPerformed(evt);
            }
        });

        BtnNewRequest.setText(" A New Request");
        BtnNewRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnNewRequestActionPerformed(evt);
            }
        });

        jLabel3.setText("VolunteerName:");

        jLabel4.setText("AvaiableTime:");

        jLabel6.setText("Search for Help Request");

        BtnContact.setText("Contact Voluntter");
        BtnContact.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnContactActionPerformed(evt);
            }
        });

        jButton1.setText("View Details");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(150, 150, 150)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(BtnBack)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(BtnCancel)
                                    .addComponent(BtnFinished)
                                    .addComponent(BtnComment)
                                    .addComponent(jButton1))
                                .addGap(83, 83, 83)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(BtnNewRequest)
                                    .addComponent(BtnContact))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 191, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ComboBoxName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(18, 18, 18)
                                        .addComponent(TxtTime, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(ComboBoxType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(50, 50, 50)
                                .addComponent(BtnSearch))
                            .addComponent(jLabel6)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(BtnView)
                                .addGap(35, 35, 35)
                                .addComponent(BtnChoose))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(217, 217, 217))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnNewRequest)
                    .addComponent(jButton1))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnCancel)
                    .addComponent(BtnContact))
                .addGap(14, 14, 14)
                .addComponent(BtnFinished, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnComment)
                .addGap(70, 70, 70)
                .addComponent(BtnBack)
                .addGap(80, 80, 80))
            .addGroup(layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(jLabel6)
                .addGap(31, 31, 31)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnView)
                    .addComponent(BtnChoose))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(ComboBoxName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(TxtTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(ComboBoxType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(BtnSearch))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void BtnContactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnContactActionPerformed
        // TODO add your handling code here:
        
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        int row =jTable1.getSelectedRow();

        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        AdditionalRequest request = (AdditionalRequest)jTable1.getValueAt(row,4);
        
        if(request.getSender()==null ||request.getReceiver()==null){
            JOptionPane.showMessageDialog(null,"Waiting for volunteers.");
            return;
        }
        
        if(request.isContact()==true){
            request.setContact(false);
        }
        userProcessContainer.add("RequestLabTestJPanel", new ContactVolunteer(userProcessContainer,request,userAccount,system));
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_BtnContactActionPerformed

    private void BtnNewRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnNewRequestActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("ToVolunteer_NewRequest", new ToVolunteer_NewRequest(userProcessContainer, userAccount, enterprise, system));
        layout.next(userProcessContainer);
    }//GEN-LAST:event_BtnNewRequestActionPerformed

    private void BtnChooseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnChooseActionPerformed
        // TODO add your handling code here:
        
        int row =TblVolunteer.getSelectedRow();

        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        AdditionalRequest s = (AdditionalRequest)TblVolunteer.getValueAt(row,3);
        
        s.setStatus("Processing");
        s.setReceiver(userAccount);
        
        userAccount.getWorkQueue().getWorkRequestList().add(s);
        
        
        
        for(WorkRequest request:userAccount.getWorkQueue().searchRelationship(userAccount.getWorkQueue().getWorkRequestList(),1)){
            System.out.println("a"+request.getSender().getUsername());
        }
        populatePatientRequestHistory();
        populateVolunteer();
        
        JOptionPane.showMessageDialog(null, "Choose successfully.Please wait for volunteer coming:)");
    }//GEN-LAST:event_BtnChooseActionPerformed

    private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
        // TODO add your handling code here:
//        userProcessContainer.remove(this);
//        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
//        layout.previous(userProcessContainer);    
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        userProcessContainer.add("workArea",userAccount.getRole().createWorkArea(userProcessContainer, userAccount, null, null, enterprise, system));
        layout.next(userProcessContainer);
    }//GEN-LAST:event_BtnBackActionPerformed

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        // TODO add your handling code here:
        int row =jTable1.getSelectedRow();

        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        WorkRequest s = (WorkRequest)jTable1.getValueAt(row,4);
        if(!s.getStatus().equals("Completed")){
            s.setStatus("Cancelled");
            populatePatientRequestHistory();
        }else if(s.getStatus().equals("Cancelled")){
            JOptionPane.showMessageDialog(null, "Already Cancelled.");
        }
        
        else{
            JOptionPane.showMessageDialog(null, "Cannot cancel.");
            return;
        }
        
    }//GEN-LAST:event_BtnCancelActionPerformed

    private void BtnFinishedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFinishedActionPerformed
        // TODO add your handling code here:
        
        int row =jTable1.getSelectedRow();

        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        WorkRequest s = (WorkRequest)jTable1.getValueAt(row,4);
        if(s.getStatus().equals("Processing")){
        s.setStatus("Completed");
        populatePatientRequestHistory();}else{
            JOptionPane.showMessageDialog(null, "Not right status.(The status should be processing.)");
            return;
        }
    }//GEN-LAST:event_BtnFinishedActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        int row =jTable1.getSelectedRow();

        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        AdditionalRequest request = (AdditionalRequest)jTable1.getValueAt(row,4);
        
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("RequestLabTestJPanel", new ToVolunteer_ViewHistory(userProcessContainer,request,userAccount));
        layout.next(userProcessContainer);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void BtnCommentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCommentActionPerformed
        // TODO add your handling code here:
        int row =jTable1.getSelectedRow();
        
        
        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        AdditionalRequest request = (AdditionalRequest)jTable1.getValueAt(row,4);
        if(request.getDescription()!=null){
            JOptionPane.showMessageDialog(null,"You have already done,plz use view.");
            return;
        }
        if(request.getStatus().equals("Completed")){
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("RequestLabTestJPanel", new ToVolunteer_Comment(userProcessContainer,request,userAccount));
        layout.next(userProcessContainer);}else {
            JOptionPane.showMessageDialog(null, "Please comment after completed", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        
        
                                           
    }//GEN-LAST:event_BtnCommentActionPerformed

    private void BtnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnViewActionPerformed
        // TODO add your handling code here:
        int row =TblVolunteer.getSelectedRow();

        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        AdditionalRequest request = (AdditionalRequest)TblVolunteer.getValueAt(row,3);
        
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("RequestLabTestJPanel", new ToVolunteer_View(userProcessContainer,request,userAccount));
        layout.next(userProcessContainer);
    }//GEN-LAST:event_BtnViewActionPerformed

    private void BtnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSearchActionPerformed
        // TODO add your handling code here:
        
        String type =(String) this.ComboBoxType.getSelectedItem();
        String acc = (String) this.ComboBoxName.getSelectedItem();
        String time =this.TxtTime.getText();
        
        DefaultTableModel model = (DefaultTableModel) TblVolunteer.getModel();
        
        model.setRowCount(0);
        
        
            
                    for (OrganizationHospital organization : enterprise.getOrganizationHospitalDirectory().getOrganizationHospitalList()){
                        if (organization instanceof PatientsCommunity){
                            for (WorkRequest request :organization.getWorkQueue().searchWaiting(organization.getWorkQueue().getWorkRequestList())){

                                
                                if((!acc.equals(""))&&(request.getSender().getUsername().equals(acc))==false ) continue;
                                if((!time.equals(""))&& ((AdditionalRequest)request).getReservationTime().equals(time)==false) continue;
                                if((!type.equals(""))&& ((AdditionalRequest)request).getWorkType().equals(type)==false) continue;
                                
                                Object[] row = new Object[4];

                                row[0]=request.getSender().getUsername();
                                row[1]=((AdditionalRequest)request).getReservationTime();
                                row[2]=((AdditionalRequest)request).getWorkType();
                                row[3]=request;
                                model.addRow(row);
                                
                            }
                        }
                    
                
            
                    }
        
    }//GEN-LAST:event_BtnSearchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnBack;
    private javax.swing.JButton BtnCancel;
    private javax.swing.JButton BtnChoose;
    private javax.swing.JButton BtnComment;
    private javax.swing.JButton BtnContact;
    private javax.swing.JButton BtnFinished;
    private javax.swing.JButton BtnNewRequest;
    private javax.swing.JButton BtnSearch;
    private javax.swing.JButton BtnView;
    private javax.swing.JComboBox<String> ComboBoxName;
    private javax.swing.JComboBox<String> ComboBoxType;
    private javax.swing.JTable TblVolunteer;
    private javax.swing.JTextField TxtTime;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
