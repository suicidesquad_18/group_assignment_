/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Userinterface.Researcher;

import Business.Enterprise.Enterprise;
import Business.MSAPlatform;
import Business.OrganizationHospital.OrganizationHospital;
import Business.OrganizationHospital.ResearchInstitution;
import Business.Roles.Researcher;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.AdditionalRequest;
import Userinterface.Resource.FileDownloadJPanel;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author wu
 */
public class ResearcherWorkArea extends javax.swing.JPanel {

    /**
     * Creates new form ResearcherWorkArea
     */
    JPanel userProcessContainer;
    AdditionalRequest request;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private MSAPlatform system;
    private OrganizationHospital organization;
    private Researcher researcher;
    public ResearcherWorkArea(JPanel userProcessContainer, UserAccount account, Enterprise enterprise, OrganizationHospital organizationHospital, MSAPlatform system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.organization=organizationHospital;
        this.system = system;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnManageProfile = new javax.swing.JButton();
        uploadPaperBtn = new javax.swing.JButton();
        btnManageDoctorRequest = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Apple Braille", 0, 24)); // NOI18N
        jLabel1.setText("Researcher Work Area");

        btnManageProfile.setBackground(new java.awt.Color(204, 255, 204));
        btnManageProfile.setFont(new java.awt.Font("Apple Braille", 0, 14)); // NOI18N
        btnManageProfile.setText("Manage Profile");
        btnManageProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageProfileActionPerformed(evt);
            }
        });

        uploadPaperBtn.setBackground(new java.awt.Color(204, 255, 204));
        uploadPaperBtn.setFont(new java.awt.Font("Apple Braille", 0, 14)); // NOI18N
        uploadPaperBtn.setText("Uploade Paper");
        uploadPaperBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uploadPaperBtnActionPerformed(evt);
            }
        });

        btnManageDoctorRequest.setBackground(new java.awt.Color(204, 255, 204));
        btnManageDoctorRequest.setFont(new java.awt.Font("Apple Braille", 0, 12)); // NOI18N
        btnManageDoctorRequest.setText("Manage Doctor Request");
        btnManageDoctorRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageDoctorRequestActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnManageDoctorRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(uploadPaperBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnManageProfile, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(494, Short.MAX_VALUE)
                        .addComponent(jLabel1)))
                .addContainerGap(634, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(123, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(btnManageProfile, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addComponent(uploadPaperBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(btnManageDoctorRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(94, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnManageDoctorRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageDoctorRequestActionPerformed
        // TODO add your handling code here:
        ManageDoctorRequest manageDoctorRequest = new ManageDoctorRequest(userProcessContainer,userAccount,organization,researcher);
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        userProcessContainer.add("ManageDoctorRequest",manageDoctorRequest);
        layout.next(userProcessContainer);
        
        
        
        
    }//GEN-LAST:event_btnManageDoctorRequestActionPerformed

    private void btnManageProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageProfileActionPerformed
        // TODO add your handling code here:
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        userProcessContainer.add("ManageDoctorRequest", new MyProfile(userProcessContainer, userAccount));
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnManageProfileActionPerformed

    private void uploadPaperBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uploadPaperBtnActionPerformed
        // TODO add your handling code here:
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        userProcessContainer.add("FileDownloadJPanel", new FileDownloadJPanel(userProcessContainer, userAccount, system));
        layout.next(userProcessContainer);
    }//GEN-LAST:event_uploadPaperBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnManageDoctorRequest;
    private javax.swing.JButton btnManageProfile;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton uploadPaperBtn;
    // End of variables declaration//GEN-END:variables
}
