/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Userinterface.VolunteerRole;

import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.MSAPlatform;
import Business.OrganizationHospital.OrganizationHospital;
import Business.OrganizationVolunteer.VolunteerOrg;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.AdditionalRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author littlelion
 */
public class VWorkQueue extends javax.swing.JPanel {

    /**
     * Creates new form WorkQueue
     */
    private JPanel rightPanel;
    private UserAccount account;
    private Enterprise  enterprise;
    private MSAPlatform platform;
    private VolunteerOrg vorg;
    public VWorkQueue(JPanel rightPanel, UserAccount account,Enterprise enterprise,MSAPlatform platform,OrganizationHospital organizationHospital) {
        initComponents();
        this.rightPanel=rightPanel;
        this.account=account;
        this.enterprise=enterprise;
        
        this.platform=platform;
        this.vorg=(VolunteerOrg)organizationHospital;
        populateTblSearch();
        populateHistory();
        populateTodo();
        addComboBox();
    }
    
    private void addComboBox(){
        this.ComboBoxType.removeAllItems();
        this.ComboBoxType.addItem("");
        this.ComboBoxType.addItem("Movement");
        this.ComboBoxType.addItem("Caring");
        this.ComboBoxType.addItem("Washing");
        this.ComboBoxType.addItem("Eating");
        this.ComboBoxType.addItem("Others");
    }
    
    private void populateHistory(){
        
        DefaultTableModel model = (DefaultTableModel) this.TblHistory.getModel();
        
        model.setRowCount(0);
        for(WorkRequest request:account.getWorkQueue().searchCompleted(account.getWorkQueue().getWorkRequestList())){
                    Object[] row = new Object[5];
              
                    row[0] = ((AdditionalRequest)request).getReservationTime();
                    String type = ((AdditionalRequest) request).getWorkType();
                    if(request.getSender()!=null && request.getReceiver()!=null){
                         row[1]=request.getReceiver()==account?request.getSender().getUsername():request.getReceiver().getUsername();
                    }
                    else{
                        row[1]="";
                    }
                    row[2] = type;
                    if(request.getStatus().equals("Cancelled")){
                        row[3]=request.getStatus();
                    }else{
                        if(((AdditionalRequest)request).getDescription()==null){
                            row[3]="no  comment";
                        }else
                        row[3]=((AdditionalRequest) request).getDescription();
                    }
                    
                    row[4]=request;
                    model.addRow(row);
        }
    }
    
    public void populateTodo(){
        DefaultTableModel model = (DefaultTableModel) this.TblTodo.getModel();
        
        model.setRowCount(0);
        for(WorkRequest request:account.getWorkQueue().searchWaiting(account.getWorkQueue().getWorkRequestList())){
                    Object[] row = new Object[6];
              
                    row[0] = ((AdditionalRequest)request).getReservationTime();
                    row[1]="Waiting";
                    String type = ((AdditionalRequest) request).getWorkType();
                    row[2] = type;
                    
                    row[3]= request.getStatus();
                    
                    row[4]=request.getMessage();
                    row[5]=request;
                    model.addRow(row);
        }
        
        for(WorkRequest request:account.getWorkQueue().getWorkRequestList()){
            
            if(request.getStatus().equals("Processing")){
                    Object[] row = new Object[6];
              
                    row[0] = ((AdditionalRequest)request).getReservationTime();
                    String type = ((AdditionalRequest) request).getWorkType();
                    row[1]=request.getReceiver()==account?request.getSender().getUsername():request.getReceiver().getUsername();
                    row[2] = type;
                    row[3]= request.getStatus();
                    if(((AdditionalRequest)request).isContact()==true){
                    row[3]=request.getStatus()+"(New message!!)";
                    }
                    row[4]=request.getMessage();
                    row[5]=request;
                    model.addRow(row);
            }
        }
        
    
    }
    
    private void populateTblSearch(){
        DefaultTableModel model = (DefaultTableModel) this.TblSearch.getModel();
        
        model.setRowCount(0);
        
       
            
            for(WorkRequest request: vorg.getWorkQueue().getWorkRequestList()){

                if(request.getStatus().equals("Sent")){

                    Object[] row = new Object[4];
              
                    row[0] = request.getSender();
                    String type = ((AdditionalRequest) request).getWorkType();
                    row[1] = type;
                    row[2]=request.getMessage();
                    row[3]= request;
                    model.addRow(row);}
            }
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtnChoose = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        TxtTime = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        TblSearch = new javax.swing.JTable();
        ComboBoxType = new javax.swing.JComboBox<>();
        BtnSearch = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TblTodo = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        TblHistory = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        BtnChoose.setText("Accept");
        BtnChoose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnChooseActionPerformed(evt);
            }
        });

        jLabel5.setText("WorkType:");

        jLabel4.setText("Need Time:");

        jLabel6.setFont(new java.awt.Font("宋体", 0, 18)); // NOI18N
        jLabel6.setText("Search for Help Request");

        TblSearch.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Patient", "WorkType", "Description", "Request"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(TblSearch);
        if (TblSearch.getColumnModel().getColumnCount() > 0) {
            TblSearch.getColumnModel().getColumn(3).setMinWidth(0);
            TblSearch.getColumnModel().getColumn(3).setPreferredWidth(0);
            TblSearch.getColumnModel().getColumn(3).setMaxWidth(0);
        }

        ComboBoxType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        BtnSearch.setText("Search");
        BtnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSearchActionPerformed(evt);
            }
        });

        jButton1.setText("New Volunteer Request");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("宋体", 0, 18)); // NOI18N
        jLabel2.setText("My History");

        TblTodo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Time", "Patient", "WorkType", "Status", "Description", "request"
            }
        ));
        jScrollPane1.setViewportView(TblTodo);
        if (TblTodo.getColumnModel().getColumnCount() > 0) {
            TblTodo.getColumnModel().getColumn(5).setMinWidth(0);
            TblTodo.getColumnModel().getColumn(5).setPreferredWidth(0);
            TblTodo.getColumnModel().getColumn(5).setMaxWidth(0);
        }

        jLabel3.setFont(new java.awt.Font("宋体", 0, 18)); // NOI18N
        jLabel3.setText("To do");

        TblHistory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Time", "Patient", "WorkType", "Comments", "request"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(TblHistory);
        if (TblHistory.getColumnModel().getColumnCount() > 0) {
            TblHistory.getColumnModel().getColumn(4).setMinWidth(0);
            TblHistory.getColumnModel().getColumn(4).setPreferredWidth(0);
            TblHistory.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        jButton2.setText("Contact Patient");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Cancel");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Back");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel1.setText("Do not find suitable request? ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jButton2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton3))
                        .addComponent(jLabel2)
                        .addComponent(jLabel3)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 562, Short.MAX_VALUE)
                        .addComponent(jScrollPane3))
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(BtnChoose)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(BtnSearch)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(TxtTime, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ComboBoxType, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(113, 131, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jSeparator1)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 44, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2)
                            .addComponent(jButton3))
                        .addGap(27, 27, 27)
                        .addComponent(jButton4)
                        .addGap(24, 24, 24))))
            .addGroup(layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(jLabel6)
                .addGap(27, 27, 27)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(BtnChoose)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(TxtTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(ComboBoxType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(BtnSearch)
                .addGap(32, 32, 32)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void BtnChooseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnChooseActionPerformed
        // TODO add your handling code here:
        int selectedRow = this.TblSearch.getSelectedRow();
        
        if (selectedRow < 0){
            return;
        }
        
        AdditionalRequest request = (AdditionalRequest)TblSearch.getValueAt(selectedRow, 3);
        request.setReceiver(account);
        request.setStatus("Processing");
        
        
        account.getWorkQueue().getWorkRequestList().add(request);
        
        populateTblSearch();
        populateTodo();
        
        JOptionPane.showMessageDialog(null, "You have Accrpted it. Thank you for your help.");
        
    }//GEN-LAST:event_BtnChooseActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) rightPanel.getLayout();
        rightPanel.add( new VolunteerNewRequest(rightPanel, account, enterprise,platform));
        layout.next(rightPanel);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        int selectedRow = this.TblTodo.getSelectedRow();
        
        if (selectedRow < 0){
            JOptionPane.showMessageDialog(null, "Please select a row.");
            return;
        }
         AdditionalRequest request = (AdditionalRequest)TblTodo.getValueAt(selectedRow, 5);
         
         
         
         
         
         request.setStatus("Cancelled");
         
         populateTodo();
         populateHistory();
         
         JOptionPane.showMessageDialog(null, "You have cancelled it.");
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) rightPanel.getLayout();
        int row =TblTodo.getSelectedRow();

        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        AdditionalRequest request = (AdditionalRequest)TblTodo.getValueAt(row, 5);
        if(request.getSender()==null ||request.getReceiver()==null){
            JOptionPane.showMessageDialog(null,"Waiting for volunteers.");
            return;
        }
        
        rightPanel.add("RequestLabTestJPanel", new ContactPatient(rightPanel,request,account,platform));
        layout.next(rightPanel);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
//        rightPanel.remove(this);
//        CardLayout layout = (CardLayout)rightPanel.getLayout();
//        layout.previous(rightPanel);   
        CardLayout layout = (CardLayout)rightPanel.getLayout();
        rightPanel.add("workArea", account.getRole().createWorkArea(rightPanel, account, vorg, null, enterprise, platform));
        layout.next(rightPanel);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void BtnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSearchActionPerformed
        // TODO add your handling code here:
        String type =(String) this.ComboBoxType.getSelectedItem();
        String time =this.TxtTime.getText();
        
        DefaultTableModel model = (DefaultTableModel) this.TblSearch.getModel();
        
        model.setRowCount(0);
        
       
            
            for(WorkRequest request: vorg.getWorkQueue().getWorkRequestList()){

                if(request.getStatus().equals("Sent")){
                    String type1 = ((AdditionalRequest) request).getWorkType();
                    if((!time.equals(""))&& ((AdditionalRequest)request).getReservationTime().equals(time)==false) continue;
                    if((!type.equals(""))&& ((AdditionalRequest)request).getWorkType().equals(type)==false) continue;
                    
                    
                    Object[] row = new Object[3];
              
                    row[0] = request.getSender();
                    
                    row[1] = type1;
                    row[2]= request;
                    model.addRow(row);}
            }
        
        
        
    }//GEN-LAST:event_BtnSearchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnChoose;
    private javax.swing.JButton BtnSearch;
    private javax.swing.JComboBox<String> ComboBoxType;
    private javax.swing.JTable TblHistory;
    private javax.swing.JTable TblSearch;
    private javax.swing.JTable TblTodo;
    private javax.swing.JTextField TxtTime;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
