/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import java.util.Date;

/**
 *
 * @author wu
 */
public class Customer extends User implements Comparable<Customer> {

    private Date createDate;

    public Customer(Date createDate, String password, String userName) {
        super(password, userName, "Customer");
        this.createDate =createDate;

    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public boolean verify(String password) {
        return password.equals(getPassword());
    }

    @Override
    public String toString() {
        return getUserName();
    }

    @Override
    public int compareTo(Customer o) {
        return o.getUserName().compareTo(this.getUserName());
    }

}
